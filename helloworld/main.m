//
//  main.m
//  helloworld
//
//  Created by iOS Developer on 22/01/14.
//  Copyright (c) 2014 UPC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool { // alliberar memoria automaticament
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class])); // Va a  lappdelegate
    }
}
