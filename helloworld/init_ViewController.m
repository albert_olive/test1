//
//  init_ViewController.m
//  helloworld
//
//  Created by iOS Developer on 22/01/14.
//  Copyright (c) 2014 UPC. All rights reserved.
//

#import "init_ViewController.h"

@interface init_ViewController ()

@end

@implementation init_ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)botonpulsado:(id)sender{
    NSLog(@"click");
}

@end
