//
//  AppDelegate.h
//  helloworld
//
//  Created by iOS Developer on 22/01/14.
//  Copyright (c) 2014 UPC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
