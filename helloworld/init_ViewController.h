//
//  init_ViewController.h
//  helloworld
//
//  Created by iOS Developer on 22/01/14.
//  Copyright (c) 2014 UPC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface init_ViewController : UIViewController{
    IBOutlet UIButton *boton; //clase private. es un Set. Es la mateixa que la dabaix
}

@property (weak, nonatomic) IBOutlet UIButton *boton; // Aquest boto vindra del storyboard, lstoryboard el veura

- (IBAction)botonpulsado:(id)sender; //les funcions comencen amb un guio "-"

@end
